#tests_arithmetic.py
import unittest
from my_arithmetic_tpuser.arithmetic import pgcd

def test_pgcd():
    assert pgcd(48, 18) == 6
    assert pgcd(60, 48) == 12

if __name__ == "__main__":
    unittest.main() 